<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Http\Exception\NotFoundException;
/**
 * Competitions Controller
 *
 * @property \App\Model\Table\CompetitionsTable $Competitions
 *
 * @method \App\Model\Entity\Competition[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CompetitionsController extends AppController
{
    

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['landing','index', 'view','list']);
    }
    public function initialize() {
        parent::initialize();
        $this->loadModel('Competitions');
        $this->loadModel('Mentors');
        $this->loadModel('Users');
        $this->loadModel('Fields');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function landing()
    {
        $competitions = $this->paginate($this->Competitions);
        $this->layout = false;
    }   

    public function index()
    {
        $competitions = $this->paginate($this->Competitions);
        $field = $this->Fields->find()
                            ->select(['name','id']);
        $role = $this->Auth->user('role');
        $this->set(compact('competitions','role','field'));
    }

    public function list($id = null)
    {
        $competition = $this->Competitions->find()
                                        ->where(['fields_id'=>$id]);

        $field = $this->Fields->find()
                    ->select(['name','id']);

        $competitions = $this->paginate($competition);
        $filter = count($competitions);
        if($filter == 0){
            $this->Flash->error(__('There is no competition in selected competition list. '));
            return $this->redirect(['action' => 'index']);
        }
        $role = $this->Auth->user('role');
        $this->set(compact('competitions','role','field'));
    }
    /**
     * View method
     *
     * @param string|null $id Competition id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $competition = $this->Competitions->get($id, [
            'contain' => ['Mentors','SecondMentors','ThirdMentors','Users']
        ]);
        $role = $this->Auth->user('role');
        $this->set(compact('competition','role'));
    }

    public function publish()
    {
        $field = $this->getField();
        $mentor = $this->getMentor(1);
        if($this->Auth->user('role') != 'admin')
        {
            return $this->redirect(['controller' => 'Competitions', 'action' => 'index']);
        }
        $competition = $this->Competitions->newEntity();
        if ($this->request->is('post')) 
        {
            if (!empty($this->request->data['image']['name'])) 
            {
                        $file = $this->request->data['image']; 

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $setNewFileName = $file['name'];
                        $arr_ext = array('jpg', 'jpeg', 'png'); 

                        if (in_array($ext, $arr_ext)) {

                            move_uploaded_file($file['tmp_name'], 'img/Competition/' . $file['name']);

                            $this->request->data['image'] = $file['name'];
                }
            }
            $competition = $this->Competitions->patchEntity($competition, $this->request->getData());
            $competition->publisher = $this->Auth->user('id');

            if ($this->Competitions->save($competition)) {
                $competition->publisher = $this->Auth->user('name');
                $this->Flash->success(__('The competition has been published.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->redirect($this->referer());
            $this->Flash->error(__('The competition could not be saved. Please, try again.'));
        }
        $this->set(compact('competition','mentor','field'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Competition id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $field = $this->getField();

        $competition = $this->Competitions->get($id, [
            'contain' => ['Users']
        ]);
        $mentor = $this->Mentors->find('list', ['KeyField' => 'id', 'valueField' => 'name'])
                            ->where(['fields_id' => $competition->fields_id]);

        $combo = [];
        foreach($mentor as $key => $value)
        {
                $combo[0] = "None";
                $combo[$key] =$value;

        }

        $mentor = $combo;
        $saveimg = $competition->image;
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            if (!empty($this->request->data['image']['name'])) 
            {
                        $file = $this->request->data['image']; 

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $setNewFileName = $file['name'];
                        $arr_ext = array('jpg', 'jpeg', 'png'); 

                        if (in_array($ext, $arr_ext)) {

                            move_uploaded_file($file['tmp_name'], 'img/Competition/' . $file['name']);

                            $this->request->data['image'] = $file['name'];
                }
            }

            $competition = $this->Competitions->patchEntity($competition, $this->request->getData());
            if ($this->Competitions->save($competition)) {
                if($competition->image == null)
                    {
                        $competition->image = $saveimg;
                        $this->Competitions->save($competition);
                    }
                $this->Flash->success(__('The competition has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->redirect($this->referer());
            $this->Flash->error(__('The competition could not be saved. Please, try again.'));
        }
        $this->set(compact('competition','mentor','field'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Competition id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $competition = $this->Competitions->get($id);
        if ($this->Competitions->delete($competition)) {
            $this->Flash->success(__('The competition has been deleted.'));
        } else {
            $this->Flash->error(__('The competition could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getField()
    {
        //$this->loadModel('Country');
        //return $this->Countries->find('list');
        $field = $this->Fields->find('list', ['KeyField' => 'id', 'valueField' => 'name']);
        return $field;

    }

    private function getMentor($fields_id = null)
    {
        $mentor = $this->Mentors->find('list', ['KeyField' => 'id', 'valueField' => 'name'])
                            ->where(['fields_id' => $fields_id]);
        return $mentor;
    }


    public function ajaxStateByField()
    {
        if ($this->request->is(['ajax', 'post'])) 
        {
            $id = $this->request->query('id');
            $mentor = $this->getMentor($id);

            $combo = [];
            foreach ($mentor as $key => $value) 
            {
                if($value != 'None')
                {
                    $combo[0] = "<option value='".'0'."'>".'None'."</option>";
                    $combo[] = "<option value='".$key."'>".$value."</option>";
                }else
                {
                    $combo[] = "<option value='".$key."'>".$value."</option>";
                }

            }            

            $data = ['data' => ['mentor' => $combo]];

        return $this->response->withType("application/json")->withStringBody(json_encode($data));

        }
    }
}
