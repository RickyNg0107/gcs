<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * Results Controller
 *
 * @property \App\Model\Table\ResultsTable $Results
 *
 * @method \App\Model\Entity\Result[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ResultsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['index', 'view', 'Publish']);
    }

    public function initialize() {
        parent::initialize();
        $this->loadModel('Users');
    }

    public function index()
    {
        $role = $this->Auth->user('role');
        $this->paginate = [
            'contain' => ['Competitions']
        ];
        $results = $this->paginate($this->Results->find()
                                    ->where(['status' => 'approve']));

        $this->set(compact('results','role'));
    }

    public function approveresult()
    {
        $role = $this->Auth->user('role');
        $results = $this->paginate($this->Results->find()
                                    ->contain(['Competitions'])
                                    ->where(['results.status' => 'pending']));

         $this->set(compact('results','role'));
    }

    /**
     * View method
     *
     * @param string|null $id Result id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $role = $this->Auth->user('role');


        if($role != null)
        {
        $result = $this->Results->find()
                                ->where(['results.id' => $id])
                                ->contain(['Competitions'])
                                ->first();
        }
        else
        {
        $result = $this->Results->find()
                                ->where(['results.id' => $id, 'results.status' => 'approve'])
                                ->contain(['Competitions'])
                                ->first();
        }


         $this->set(compact('result', 'role'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function publish()
    {
        $role = $this->Auth->user('role');
        $result = $this->Results->newEntity();
        if ($this->request->is('post')) {
            if (!empty($this->request->data['image']['name'])) 
            {
                        $file = $this->request->data['image']; 

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $setNewFileName = $file['name'];
                        $arr_ext = array('jpg', 'jpeg', 'png'); 

                        if (in_array($ext, $arr_ext)) {

                            move_uploaded_file($file['tmp_name'], 'img/Result/' . $file['name']);

                            $this->request->data['image'] = $file['name'];
                }
            }

            if (!empty($this->request->data['image2']['name'])) 
            {
                        $file = $this->request->data['image2']; 

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $setNewFileName = $file['name'];
                        $arr_ext = array('jpg', 'jpeg', 'png'); 

                        if (in_array($ext, $arr_ext)) {

                            move_uploaded_file($file['tmp_name'], 'img/Result/' . $file['name']);

                            $this->request->data['image2'] = $file['name'];
                }
            }

            if (!empty($this->request->data['image3']['name'])) 
            {
                        $file = $this->request->data['image3']; 

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $setNewFileName = $file['name'];
                        $arr_ext = array('jpg', 'jpeg', 'png'); 

                        if (in_array($ext, $arr_ext)) {

                            move_uploaded_file($file['tmp_name'], 'img/Result/' . $file['name']);

                            $this->request->data['image3'] = $file['name'];
                }
            }
            $result = $this->Results->patchEntity($result, $this->request->getData());
            $result->publisher = $this->Auth->user('id');
            if ($this->Results->save($result)) 
            {
                $this->Flash->success(__('The result has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->redirect($this->referer());
            $this->Flash->error(__('The result could not be saved. Please, try again.'));
        }
        $competitions = $this->Results->Competitions->find('list', ['limit' => 200])->order(['created'=>'DESC']);
        $this->set(compact('result', 'competitions', 'role'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Result id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $role = $this->Auth->user('role');
        $result = $this->Results->get($id, [
            'contain' => []
        ]);
        $saveimg = $result->image;
        $saveimg2 = $result->image2;
        $saveimg3 = $result->image3;
        if ($this->request->is(['patch', 'post', 'put'])) {
                        if (!empty($this->request->data['image']['name'])) 
            {
                        $file = $this->request->data['image']; 

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $setNewFileName = $file['name'];
                        $arr_ext = array('jpg', 'jpeg', 'png'); 

                        if (in_array($ext, $arr_ext)) {

                            move_uploaded_file($file['tmp_name'], 'img/Result/' . $file['name']);

                            $this->request->data['image'] = $file['name'];
                }
            }

            if (!empty($this->request->data['image2']['name'])) 
            {
                        $file = $this->request->data['image2']; 

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $setNewFileName = $file['name'];
                        $arr_ext = array('jpg', 'jpeg', 'png'); 

                        if (in_array($ext, $arr_ext)) {

                            move_uploaded_file($file['tmp_name'], 'img/Result/' . $file['name']);

                            $this->request->data['image2'] = $file['name'];
                }
            }

            if (!empty($this->request->data['image3']['name'])) 
            {
                        $file = $this->request->data['image3']; 

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $setNewFileName = $file['name'];
                        $arr_ext = array('jpg', 'jpeg', 'png'); 

                        if (in_array($ext, $arr_ext)) {

                            move_uploaded_file($file['tmp_name'], 'img/Result/' . $file['name']);

                            $this->request->data['image3'] = $file['name'];
                }
            }
            $result = $this->Results->patchEntity($result, $this->request->getData());
            if ($this->Results->save($result)) 
            {
                if($result->image == null)
                    {
                        $result->image = $saveimg;
                        $this->Results->save($result);
                    }

                if($result->image2 == null)
                    {
                        $result->image2 = $saveimg2;
                        $this->Results->save($result);
                    }

                if($result->image3 == null)
                    {
                        $result->image3 = $saveimg3;
                        $this->Results->save($result);
                    }
                $this->Flash->success(__('The result has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->redirect($this->referer());
            $this->Flash->error(__('The result could not be saved. Please, try again.'));
        }
        $competitions = $this->Results->Competitions->find('list', ['limit' => 200]);
        $this->set(compact('result', 'competitions','role'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Result id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $result = $this->Results->get($id);
        if ($this->Results->delete($result)) {
            $this->Flash->success(__('The result has been deleted.'));
        } else {
            $this->Flash->error(__('The result could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }


    public function xls($id, $output_type = 'D')
     {
        if($this->Auth->user('role') != 'admin')
        {
            return $this->redirect(['controller' => 'Competitions', 'action' => 'index']);
        }

        $results = $this->Results->find('all', array(
            'contain'=>array('Users', 'Competitions'=>array('Fields','Mentors','SecondMentors','ThirdMentors'))))->where(['results.id'=> $id]);

        foreach($results as $result)
        {
                        if(filter_var($result->competitionname, FILTER_VALIDATE_INT)){ 

                            $file =  $result->competition->name. ' _ResultReport.xlsx';
                        }
                        else
                        { 
                            $file = $result->competitionname;
                        } 
        }


        $this->set(compact('results', 'output_type', 'file'));
        $this->viewBuilder()->layout('xls/default');
        $this->viewBuilder()->template('xls/spreadsheet_result');
        $this->RequestHandler->respondAs('xlsx');
        $this->render();

    }

    public function allxls($output_type = 'D')
     {
        if($this->Auth->user('role') != 'admin')
        {
            return $this->redirect(['controller' => 'Competitions', 'action' => 'index']);
        }
        $results = $this->Results->find('all', array('contain'=>array('Users','Competitions'=>array('Fields','Mentors','SecondMentors','ThirdMentors'))));

        $file = 'FULL_ResultReport.xlsx';
        $this->set(compact('results', 'output_type', 'file'));
        $this->viewBuilder()->layout('xls/default');
        $this->viewBuilder()->template('xls/spreadsheet_allresult');
        $this->RequestHandler->respondAs('xlsx');
        $this->render();

    }
}
