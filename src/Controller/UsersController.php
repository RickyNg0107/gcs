<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;

class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('test','register', 'logout','landing');
    }


    public function index()
    {
        $this->layout = false;

        if($this->request->getSession()->read('Auth')){
             return $this->redirect(['controller'=>'Competitions','action' => 'landing']);
        }
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if(!$user){
            $this->Flash->error(__('Wrong Username or Password'));
            return $this->redirect(['action' => 'index']);

            }
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }


    public function register()
    {
        if($this->Auth->user('role') != 'admin')
        {
            return $this->redirect(['controller' => 'Competitions', 'action' => 'index']);
        }

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            // Prior to 3.4.0 $this->request->data() was used.
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->role = 'admin';
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }

    public function edit($id = null)
    {
        if($this->Auth->user('role') != 'admin')
        {
            return $this->redirect(['controller' => 'Competitions', 'action' => 'index']);
        }
        $user = $this->Users->find()
                        ->where(['id'=>$id])
                        ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The users has been Updated.'));

                return $this->redirect(['controller' => 'Competitions', 'action' => 'index']);
            }
            $this->Flash->error(__('The users could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function logout()
    {

        $this->Auth->logout();
        $this->request->session()->destroy();
        return $this->redirect(['controller' => 'Competitions', 'action' => 'landing']);
    }

}