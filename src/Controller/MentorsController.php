<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Mentors Controller
 *
 * @property \App\Model\Table\MentorsTable $Mentors
 *
 * @method \App\Model\Entity\Mentor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MentorsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(Event $event) {
        if($this->Auth->user('role') != 'admin')
        {
            return $this->redirect(['controller' => 'Competitions', 'action' => 'index']);
        }
        $this->loadModel('Fields');
        $this->Auth->allow();
    }

    public function index()
    {
        $field = $this->Fields->find('list', ['KeyField' => 'id', 'valueField' => 'name']);
        $mentors = $this->paginate($this->Mentors->find()
                                                ->contain(['Fields']));

        $this->set(compact('mentors','field'));
    }


    public function add()
    {
        $field = $this->Fields->find('list', ['KeyField' => 'id', 'valueField' => 'name']);
        $mentor = $this->Mentors->newEntity();
        if ($this->request->is('post')) {
            $mentor = $this->Mentors->patchEntity($mentor, $this->request->getData());
            if ($this->Mentors->save($mentor)) {
                $this->Flash->success(__('The mentor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mentor could not be saved. Please, try again.'));
        }
        $this->set(compact('mentor','field'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Mentor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $field = $this->Fields->find('list', ['KeyField' => 'id', 'valueField' => 'name']);
        $mentor = $this->Mentors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mentor = $this->Mentors->patchEntity($mentor, $this->request->getData());
            if ($this->Mentors->save($mentor)) {
                $this->Flash->success(__('The mentor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mentor could not be saved. Please, try again.'));
        }
        $this->set(compact('mentor','field'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Mentor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mentor = $this->Mentors->get($id);
        if ($this->Mentors->delete($mentor)) {
            $this->Flash->success(__('The mentor has been deleted.'));
        } else {
            $this->Flash->error(__('The mentor could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function isAuthorized($user)
    {
        // Any registered user can access public functions
        if (!$this->request->getParam('prefix')) {

            return true;
        }
    }
}
