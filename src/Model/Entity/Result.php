<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Result Entity
 *
 * @property int $id
 * @property string $competitionname
 * @property string $studentname
 * @property string $studentid
 * @property string $studentphone
 * @property string $email
 * @property string $image
 * @property string $image2
 * @property string $image3
 * @property string $placement
 * @property string $description
 * @property int $publisher
 * @property string $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Competition $competition
 */
class Result extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'competitionname' => true,
        'studentname' => true,
        'studentid' => true,
        'studentphone' => true,
        'email' => true,
        'image' => true,
        'image2' => true,
        'image3' => true,
        'placement' => true,
        'description' => true,
        'publisher' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'competition' => true
    ];
}
