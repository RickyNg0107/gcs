<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Competition Entity
 *
 * @property int $id
 * @property int $publisher
 * @property string $image
 * @property string $name
 * @property int $fields_id
 * @property string $venue
 * @property \Cake\I18n\FrozenDate $date
 * @property string $description
 * @property string $link
 * @property float $budget
 * @property int $mentor
 * @property int $mentor2
 * @property int $mentor3
 * @property int $num_participants
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Mentor $Mentors
 * @property \App\Model\Entity\Mentor $Mentors2
 * @property \App\Model\Entity\Mentor $Mentors3
 * @property \App\Model\Entity\User $user
 */
class Competition extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'publisher' => true,
        'image' => true,
        'name' => true,
        'fields_id' => true,
        'venue' => true,
        'date' => true,
        'description' => true,
        'link' => true,
        'budget' => true,
        'mentor' => true,
        'mentor2' => true,
        'mentor3' => true,
        'num_participants' => true,
        'created' => true,
        'modified' => true,
        'Mentors' => true,
        'Mentors2' => true,
        'Mentors3' => true,
        'user' => true
    ];
}
