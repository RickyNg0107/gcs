<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Results Model
 *
 * @method \App\Model\Entity\Result get($primaryKey, $options = [])
 * @method \App\Model\Entity\Result newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Result[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Result|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Result|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Result patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Result[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Result findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ResultsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('results');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Competitions', [
        'foreignKey' => 'competitionname',
    ]);
        $this->belongsTo('Users', [
        'foreignKey' => 'publisher',
    ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('competitionname')
            ->maxLength('competitionname', 45)
            ->requirePresence('competitionname', 'create')
            ->notEmpty('competitionname');

        $validator
            ->scalar('studentname')
            ->maxLength('studentname', 45)
            ->requirePresence('studentname', 'create')
            ->notEmpty('studentname');

        $validator
            ->scalar('studentid')
            ->maxLength('studentid', 45)
            ->requirePresence('studentid', 'create')
            ->notEmpty('studentid');

        $validator
            ->scalar('studentphone')
            ->maxLength('studentphone', 45)
            ->requirePresence('studentphone', 'create')
            ->notEmpty('studentphone');

        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('image')
            ->maxLength('image', 45)
            ->allowEmpty('image');

        $validator
            ->scalar('image2')
            ->maxLength('image2', 45)
            ->allowEmpty('image2');

        $validator
            ->scalar('image3')
            ->maxLength('image3', 45)
            ->allowEmpty('image3');

        $validator
            ->scalar('placement')
            ->maxLength('placement', 45)
            ->requirePresence('placement', 'create')
            ->notEmpty('placement');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->integer('publisher')
            ->allowEmpty('publisher');

        $validator
            ->scalar('status')
            ->maxLength('status', 45)
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
}
