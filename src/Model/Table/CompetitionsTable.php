<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Competitions Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Fields
 *
 * @method \App\Model\Entity\Competition get($primaryKey, $options = [])
 * @method \App\Model\Entity\Competition newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Competition[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Competition|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Competition|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Competition patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Competition[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Competition findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CompetitionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('competitions');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Fields', [
            'foreignKey' => 'fields_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Mentors', [
            'foreignKey' => 'mentor',
            'propertyName' => 'Mentors',
            'joinType' => 'INNER',
            'className' => 'Mentors'
        ]);

        $this->belongsTo('SecondMentors', [
            'foreignKey' => 'mentor2',
            'propertyName' => 'Mentors2',
            'joinType' => 'INNER',
            'className' => 'Mentors'
        ]);

        $this->belongsTo('ThirdMentors', [
            'foreignKey' => 'mentor3',
            'propertyName' => 'Mentors3',
            'joinType' => 'INNER',
            'className' => 'Mentors'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'publisher',
            'joinType' => 'INNER',
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('publisher')
            ->requirePresence('publisher', 'create')
            ->notEmpty('publisher');

        $validator
            ->scalar('image')
            ->maxLength('image', 45)
            ->allowEmpty('image');

        $validator
            ->scalar('name')
            ->maxLength('name', 60)
            ->allowEmpty('name');

        $validator
            ->scalar('venue')
            ->maxLength('venue', 255)
            ->allowEmpty('venue');

        $validator
            ->date('date')
            ->allowEmpty('date');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->add('link','custom',[
                'rule' => 'url',
                'message' => 'The link must include HTTP/HTTPS'])
            ->scalar('link')
            ->maxLength('link', 45)
            ->allowEmpty('link');

        $validator
            ->numeric('budget')
            ->allowEmpty('budget');

        $validator
            ->integer('mentor')
            ->allowEmpty('mentor');

        $validator
            ->integer('mentor2')
            ->allowEmpty('mentor2');

        $validator
            ->integer('mentor3')
            ->allowEmpty('mentor3');

        $validator
            ->integer('num_participants')
            ->allowEmpty('num_participants');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fields_id'], 'Fields'));
        $rules->add($rules->existsIn(['publisher'], 'Users'));
        $rules->add($rules->existsIn(['mentor'], 'Mentors'));
        $rules->add($rules->existsIn(['mentor2'], 'SecondMentors'));
        $rules->add($rules->existsIn(['mentor2'], 'ThirdMentors'));

        return $rules;
    }
}
