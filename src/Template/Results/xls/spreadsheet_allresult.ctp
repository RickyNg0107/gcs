<?php
require_once(ROOT . DS . 'vendor' . DS . "phpoffice". DS . "phpexcel" . DS . "Classes" . DS . "PHPExcel.php");

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("creator name");

//HEADER
$i=1;
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'No.');
$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'Competition Name');
$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Placement');
$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Publisher');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'Schools');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'Venue');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, 'Date');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, 'Description');
$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, 'Event Link');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, 'Budget');
$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, 'Mentor 1');
$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, 'Mentor 2');
$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, 'Mentor 3');
$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, 'Total Pax');


//DATA

foreach ($results as $result){
$i++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $i-1);
if(filter_var($result->competitionname, FILTER_VALIDATE_INT)){ 

        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $result->competition->name);
    }
    else
    { 
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $result->competitionname);
    } 

$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $result->placement);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $result->user->name);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $result->competition->field->name);
$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $result->competition->venue);
$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $result->competition->date);
$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $result->competition->description);
$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $result->competition->link);
$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $result->competition->budget);
$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $result->competition->Mentors->name);
$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $result->competition->Mentors2->name);
$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $result->competition->Mentors3->name);
$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $result->competition->num_participants);
}

/*
if u have a collection of users just loop
//DATA
foreach($users as $user){
    $i++;
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $user->id);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $user->name);
}
*/

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Results');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

//call the function in the controller with $output_type = F and $file with complete path to the file, to generate the file in the server for example attach to email
if (isset($output_type) && $output_type == 'F') {
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save($file);
 } else {
    // Redirect output to a client's web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$file.'"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
}