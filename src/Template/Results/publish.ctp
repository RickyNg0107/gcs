<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Result $result
 */
$selection = ['1st'=>'Champion','2nd'=>'Runner Up','3rd'=>'Third Placing','4th'=>'Consolidation Prize'];
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Show All Results'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="results form large-9 medium-8 columns content">
    <?= $this->Form->create($result, ['type' => 'file']) ?>
    <fieldset>
        <?php if($this->request->getSession()->read('Auth')) { ?>
            <legend style="color: black;"><?= __('Update Competition Result') ?></legend><br>
        <?php } else { ?>
            <legend style="color: black;"><?= __('Results Submission') ?></legend><br>
        <?php } ?>

        <?php
            if($role != null)
            {
                echo $this->Form->control('competitionname', ['options' => $competitions,'label'=>'Competition Name']);
                echo $this->Form->input('studentname', ['type'=>'hidden', 'value'=>'nope']);
                echo $this->Form->input('studentid', ['type'=>'hidden', 'value'=>'nope']);
                echo $this->Form->input('studentphone', ['type'=>'hidden', 'value'=>'nope']);
                echo $this->Form->input('email', ['type'=>'hidden', 'value'=>'nope']);
            }
            else
            {
                echo $this->Form->control('competitionname', ['label'=>'Competition Name']);
                echo $this->Form->control('studentname', ['label'=>'Student Name']);
                echo $this->Form->control('studentid', ['label' => 'Student ID']);
                echo $this->Form->control('studentphone', ['label' => 'Student Phone Number']);
                echo $this->Form->control('email', ['label'=>'Student Email','email']);
            }

                echo $this->Form->input('image', array('type' => 'file','label'=>'Image','required'));
                echo $this->Form->input('image2', array('type' => 'file','label'=>'Image 2 (additional)'));
                echo $this->Form->input('image3', array('type' => 'file','label'=>'Image 3 (additional)'));
                echo $this->Form->input('placement',  array("options"=>$selection,'class' => 'form-control','label'=>'Placement', 'id'=>'select'));
                echo $this->Form->input('Others',['type'=>'checkbox','class'=>'click','label'=>'Others placement']);
                echo $this->Form->input('placement', array('label'=>'Others, please state','id'=>'newother'));
                echo $this->Form->control('description');

            if($role != null)
            {
                echo $this->Form->radio
                (
                    'status',
                    [
                        ['value' => 'pending', 'text' => 'Pending'],
                        ['value' => 'approve', 'text' => 'Approve'],
                    ]
                );
            } 

        ?>

    </fieldset>

    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script>

var button = document.getElementById("select");
var others = document.getElementById("newother");
var clickBtn = document.getElementsByClassName('click')[0];

// Disable the button on initial page load
button.disabled = false;
newother.disabled = true;

//add event listener
clickBtn.addEventListener('click', function(event) {
    button.disabled = !button.disabled;
    newother.disabled = !newother.disabled;

});


</script>