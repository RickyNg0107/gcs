<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Result $result
 */
$selection = ['1st'=>'Champion','2nd'=>'Runner Up','3rd'=>'Third Placing','4th'=>'Consolidation Prize'];
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $result->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $result->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Results'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="results form large-9 medium-8 columns content">
    <?= $this->Form->create($result, ['type' => 'file']) ?>
    <fieldset>
        <legend style="color: black;"><?= __('Edit Result') ?></legend>
        <?php
            if(filter_var($result->competitionname, FILTER_VALIDATE_INT))
            {
                echo $this->Form->control('competitionname', ['options' => $competitions,'label'=>'Competition Name','disabled']);
            }
            else
            { 
                echo $this->Form->control('competitionname', ['label'=>'Competition Name']);
            } 

            echo $this->Form->control('studentname',['label'=>'Student Name']);
            echo $this->Form->control('studentid',['label'=>'Student ID']);
            echo $this->Form->control('studentphone',['label' => 'Student Phone Number']);
            echo $this->Form->control('email',['label'=>'Student Email','type'=>'text']);

            //image 1 name
            if($result->image == null)
            {
                $imagename = 'No Image';
            }
            else
            {
                $imagename = $result->image;
            }
            //image 2 name
            if($result->image2 == null)
            {
                $imagename2 = 'No Image';
            }
            else
            {
                $imagename2 = $result->image2;
            }

            //image 3 name
            if($result->image3 == null)
            {
                $imagename3 = 'No Image';
            }
            else
            {
                $imagename3 = $result->image3;
            }

            echo $this->Form->control('image', array('type' => 'file','label'=>'Image | Image Name: '.$imagename));
            echo $this->Form->control('image2', array('type' => 'file','label'=>'Second Image | Image Name: '. $imagename2));
            echo $this->Form->control('image3', array('type' => 'file','label'=>'Third Image | Image Name: '.$imagename3));
            echo $this->Form->input('placement',  array("options"=>$selection,'class' => 'form-control','label'=>'Placement', 'id'=>'select'));
            echo $this->Form->input('Others',['type'=>'checkbox','class'=>'click','label'=>'Others placement']);
            echo $this->Form->input('placement', array('label'=>'New other placement','id'=>'newother'));
            echo $this->Form->control('description');
            if($role != null)
            {
                echo $this->Form->radio
                (
                    'status',
                    [
                        ['value' => 'pending', 'text' => 'Pending'],
                        ['value' => 'approve', 'text' => 'Approve'],
                    ]
                );
            } 
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script>

var button = document.getElementById("select");
var others = document.getElementById("newother");
var clickBtn = document.getElementsByClassName('click')[0];

// Disable the button on initial page load
button.disabled = false;
newother.disabled = true;

//add event listener
clickBtn.addEventListener('click', function(event) {
    button.disabled = !button.disabled;
    newother.disabled = !newother.disabled;

});



</script>