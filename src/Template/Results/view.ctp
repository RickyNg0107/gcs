<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Result $result
 */
?>

<?php if($role != null){ ?>
<nav class="large-3 medium-4 columns " id="actions-sidebar" >
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Show All Results'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Edit current'), ['action' => 'edit', $result->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete current'), ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete?')]) ?> </li>
        <li><?= $this->Html->link(__('Generate Report'), ['action' => 'xls', $result->id], ['confirm' => __('Are you sure you want to Generate Report?')]) ?> </li>
    </ul>
</nav>
<?php } ?>


<?php if($role != null){ ?>
    <div class="results view large-9 medium-8 columns content">
<?php } else { ?>
    <div class="results view large-12 medium-8 columns content">
<?php } ?>


    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Competition Name') ?></th>
                <?php if(filter_var($result->competitionname, FILTER_VALIDATE_INT)){ ?>
                        <td><?= $result->has('competition') ? $this->Html->link($result->competition->name, ['controller' => 'Competitions', 'action' => 'view', $result->competition->id]) : '' ?></td>
                <?php }else{ ?>
                        <td><?= h($result->competitionname) ?></td>
                <?php } ?>
        </tr>
        <tr>
            <th scope="row"><?= __('Placement') ?></th>
            <td><?= h($result->placement) ?></td>
        </tr>

        <?php if($result->image != ''){ ?>
        <tr>
            <?= $this->Html->image(h('Result/'.$result->image),['class'=>'center','style' => 'height:300px; width:340px;']) ?>
        </tr>
        <?php } ?>

        <?php if($result->image2 != ''){ ?>
            <tr>
                <?= $this->Html->image(h('Result/'.$result->image2),['class'=>'center','style' => 'height:300px; width:350px;']) ?>
            </tr>
        <?php } ?>
        
        <?php if($result->image3 != ''){ ?>
            <tr>
                <?= $this->Html->image(h('Result/'.$result->image3),['class'=>'center','style' => 'height:300px; width:350px;']) ?>
            </tr>
        <?php } ?>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <div style="color:black">
            <?= $this->Text->autoParagraph(h($result->description)); ?>
        </div>
    </div>
</div>
