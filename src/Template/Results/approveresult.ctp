<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Result[]|\Cake\Collection\CollectionInterface $results
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Show All Results'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="results index large-9 medium-8 columns content">
    <h3><?= __('Pending Results') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('No') ?></th>
                <th scope="col"><?= $this->Paginator->sort('competitionname',['label'=>'Competition Name']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('studentname',['label'=>'Student Name']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('studentid',['label'=>'Student ID']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('placement') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($results as $result): ?>
            <tr>
                <td><p " "></p></td>
                <?php if(filter_var($result->competitionname, FILTER_VALIDATE_INT)){ ?>
                        <td><?= $result->has('competition') ? $this->Html->link($result->competition->name, ['controller' => 'Competitions', 'action' => 'view', $result->competition->id]) : '' ?></td>
                <?php }else{ ?>
                        <td><?= h($result->competitionname) ?></td>
                <?php } ?>

                <td><?= h($result->studentname) ?></td>
                <td><?= h($result->studentid) ?></td>
                <td><?= h($result->email) ?></td>

                <?php if($result->image != null){ ?>
                    <td><?= $this->Html->image(h('Result/'.$result->image),['url' => ['controller' => 'results', 'action' => 'view', $result->id], 'style' => 'height:50px; width:50px;']) ?></td>
                <?php }else{ ?>
                <td><?php echo 'No IMG' ?></td>
                <?php } ?>
                <td><?= h($result->placement) ?></td>
                <td><?= h($result->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $result->id]) ?>
                    <?php if($role != null){ ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $result->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->id)]) ?>
                    <?php } ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<script>


    var table = document.getElementsByTagName('table')[0],
        rows = table.getElementsByTagName('tr'),
        text = 'textContent' in document ? 'textContent' : 'innerText';
    console.log(text);

    for (var i = 1, len = rows.length; i < len; i++){
        rows[i].children[0][text] = i + rows[i].children[0][text];
    }

</script>