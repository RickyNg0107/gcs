<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>APU Competition Achievements System</title>

    <link href="/gcs/css/bootstrap.min.css" rel="stylesheet">
    <link href="/gcs/css/landing-page.css" rel="stylesheet">
    <link href="/gcs/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">



<style>
/* Dropdown Button */
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">



<?php if($this->request->getSession()->read('Auth')) { ?>
<div class="dropdown">
  <button class="dropbtn">SERVICES</button>
  <div class="dropdown-content">
    <a href="/gcs/competitions/publish">Publish New Competition</a>
    <a href="/gcs/results/publish">Update Competition Result</a>
    <a href="/gcs/results/approveresult">View Pending Results</a>
    <a href="/gcs/mentors/add">Define New Mentor</a>
    <a href="/gcs/fields/add">Define New School</a>
    <a href="/gcs/users/register">Register New Account</a>
    <a href="/gcs/users/edit/">Edit Account</a>
  </div>

  <?php } ?>
</div>

<?php if($this->request->getSession()->read('Auth')) { ?>
<div class="dropdown">
  <button type = "submit" class="dropbtn"><a href="/gcs/users/logout" style="color:white;">LOGOUT</button>
  <?php } else { ?>
  <button type = "submit" class="dropbtn"><a href="/gcs/results/publish" style="color:white;">SUBMIT RESULT</a></button>
  <button type = "submit" class="dropbtn"><a href="/gcs/users" style="color:white;">LOGIN</button>
  <?php } ?>
</div>




            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="https://notepad.pw/gcsTP038164">GROUP <strong>(TP038164 - APT2F1708CYB)</strong></a>
            </div>           
        </div>
    </nav>

    <!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h2 class="">APU Competitions Achievement System</h2>
                        <h3 class=""><i>“Strive for progress, not perfection.”</i></h3>
                        <?php if($this->request->getSession()->read('Auth')) { ?>
                            <br><h4 class="">A D M I N &emsp; P A N E L</h4>
                        <?php } ?>

                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                            <?php if($this->request->getSession()->read('Auth')) { ?>
                                <li>
                                    <a href="/gcs/competitions/" class="btn btn-default btn-lg"><span class="network-name">Competitions</span></a>
                                </li>
                                <li>
                                    <a href="/gcs/results" class="btn btn-default btn-lg"><span class="network-name">Results</span></a>
                                </li>
                                <li>
                                    <a href="/gcs/mentors" class="btn btn-default btn-lg"><span class="network-name">Mentors</span></a>
                                </li>
                                <li>
                                    <a href="/gcs/fields" class="btn btn-default btn-lg"><span class="network-name">Schools</span></a>
                                </li>

                            <?php } else { ?>
                                <li>
                                    <a href="/gcs/competitions/" class="btn btn-default btn-lg"><span class="network-name">Competitions</span></a>
                                </li>
                                <li>
                                    <a href="/gcs/results" class="btn btn-default btn-lg"><span class="network-name">Results</span></a>
                                </li>
                            <?php } ?>

                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

	<a  name="services"></a>

    <div class="content-section-b">

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Find everything you need<br><br>HERE</h2>
                    <p class="lead">Introducing the informational based system that contain all the ongoing competitions, post-competition results, and you may even submit your external competition result through this platform! <br><br><i>Designed and built by </i><strong>APU students</strong>, for <strong>APU students</strong>.</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="/gcs/img/post.jpg" alt="" style="width:730px; height: 400px;">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">I found it all<br>@APU</h2>
                    <p class="lead">Team <strong>"Business Students"</strong> found it all at the <strong>KPMG Cyber Security Challenge 2018</strong>! <br><br>So are you ready for the exciting journey ahead?<br><br>
                        <a target="_blank" href="http://www.apu.edu.my/media/news/1680?fbclid=IwAR1N0IfehcY-jB4dRID2h6ia7zqx74QKxP8snzU8iPmYdVHS96TREio90PI"><strong>Read more</strong></a></p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="/gcs/img/post2.jpg" alt="" style="width:730px; height: 550px;">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->

	<a  name="contact"></a>
    <div class="banner">

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2 style="text-decoration: bold; margin-left: -20%;">Connect with us</h2>
                </div>
                <div class="col-lg-7" style="margin-left: -15%;">
                    <ul class="list-inline banner-social-buttons">
                        <li>
                            <a href="https://www.facebook.com/apuniversity/" class="btn btn-default btn-lg"><i class="fa fa-facebook fa-fw"></i> <span class="network-name">Facebook</span></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/AsiaPacificU" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/school/asia-pacific-university-of-technology-and-innovation-apu-apiit-/" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.banner -->

    <!-- Footer -->

    <footer style="background-color: #C5DCDE;">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <ul class="list-inline">
                        <li>
                            <a href="http://www.apu.edu.my/explore-apu/university">About</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="http://www.apu.edu.my/explore-apu/contact-us">Contact</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="https://notepad.pw/gcsTP038164">FAQ</a>
                        </li>
                    </ul>
                
                <span style="font-size: 10px; text-align: center;">
                    <span style="font-family: arial,helvetica,sans-serif;">
                        <strong>APU - Rated No.1&nbsp;in Asia and Malaysia for Multicultural Learning Experience. </strong>
                        <br>(*Student Barometer Wave 2017, ‘Studying with people from other cultures’)<br>
                        <strong>APU - Among Highest Rated Emerging Malaysian Universities - 5 STAR (Excellent Rating) -&nbsp; SETARA 2017 </strong>
                    </span>
                </span>
                
                    <p class="copyright text-muted small" style="color:black;">Copyright &copy; Group TP038164 (Group Case Study) <br><br>All Rights Reserved</p>
                </div>
            </div>
        </div>

    </footer>

    <!-- jQuery -->
    <script src="/gcs/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/gcs/js/bootstrap.min.js"></script>

</body>
</html>
