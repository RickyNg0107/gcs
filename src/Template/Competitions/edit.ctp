<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Competition $competition
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $competition->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $competition->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Show All Listing'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="competitions form large-9 medium-8 columns content">
    <?= $this->Form->create($competition, ['type' => 'file']) ?>
    <fieldset>
        <legend style="color: black;"><?= __('Edit Competition') ?></legend>
        <br>
        <h5>Publisher: <?php echo $competition->user->name ?></h5>
        <?php

            //image 1 name
            if($competition->image == null)
            {
                $imagename = 'No Image';
            }
            else
            {
                $imagename = $competition->image;
            }
            echo $this->Form->control('image', array('type' => 'file','label'=>'Image | Image Name: '. $imagename));
            echo $this->Form->control('name',['label'=>'Competition Name']);
            $request = $this->Url->build(['action' => 'ajaxStateByField', 'ext' => 'json']);
            echo $this->Form->control('fields_id', ['id' => 'fieldField', 
                                               'rel' => $request,
                                               'options' => $field]);
            echo $this->Form->control('venue');
            echo $this->Form->control('date', ['empty' => true]);
            echo $this->Form->control('description');
            echo $this->Form->control('link');
            echo $this->Form->control('budget');
            echo $this->Form->control('mentor', ['id' => 'mentorField','options' => $mentor]);
            echo $this->Form->control('mentor2', ['id' => 'mentor2Field','options' => $mentor]);
            echo $this->Form->control('mentor3', ['id' => 'mentor3Field','options' => $mentor]);
            echo $this->Form->control('num_participants', ['label'=>'Number of Participants']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script>
    $(function() 
{
    $('#fieldField').change(function() 
    {
        var targeturl = $(this).attr('rel');
        var field_id = $(this).val();

        $.ajax({
            type: 'get',
            url: targeturl + '&id=' + field_id,
            beforeSend: function(xhr) 
            {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) 
            {
                if (response.data) 
                {
                    $('#mentorField').html(response.data.mentor);
                    $('#mentor2Field').html(response.data.mentor);
                    $('#mentor3Field').html(response.data.mentor);
                }
            },
        });
    });
});
</script>