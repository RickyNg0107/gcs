<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Competition $competition
 */
?>        


<?php if($role != null){ ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Show All Competition'), ['action' => 'index']) ?> </li>
            <li><?= $this->Html->link(__('Edit current'), ['action' => 'edit', $competition->id]) ?> </li>
            <li><?= $this->Form->postLink(__('Delete current'), ['action' => 'delete', $competition->id], ['confirm' => __('Are you sure you want to delete # {0}?', $competition->id)]) ?> </li>
    </ul>
</nav>
<?php } ?>

<?php if($role != null){ ?>
    <div class="results view large-9 medium-8 columns content">
<?php } else { ?>
    <div class="results view large-12 medium-8 columns content">
<?php } ?>

    <h3><?= h($competition->name) ?></h3>
        <div class="center">
            <?= $this->Html->image(h('Competition/'.$competition->image),['class'=>'center','style' => 'height:500px; width:500px;']) ?>
        </div>
    <table class="vertical-table">

        <tr>
            <th scope="row"><?= __('Venue') ?></th>
            <td><?= h($competition->venue) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Link') ?></th>
            <td><?= h($competition->link) ?></td>
        </tr>
        <?php if($role != null){ ?>
            <tr>
                <th scope="row"><?= __('Publisher') ?></th>
                <td><?= h($competition->user['name']) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Budget') ?></th>
                <td><?= $this->Number->format($competition->budget) ?></td>
            </tr>
        <?php } ?>
        <tr>
            <th scope="row"><?= __('Mentor') ?></th>
            <td><?= h($competition->Mentors['name'].' | Phone: '.$competition->Mentors['phone']) ?></td>
        </tr>
        
        <?php if($competition->Mentors2->id != '1'){ ?>
            <tr>
                <th scope="row"><?= __('Second Mentor') ?></th>
                <td><?= h($competition->Mentors2['name'].' | Phone: '.$competition->Mentors2['phone']) ?></td>
            </tr>
        <?php } ?>

        <?php if($competition->Mentors3->id != '1'){ ?>
            <tr>
                <th scope="row"><?= __('Third Mentor') ?></th>
                <td><?= h($competition->Mentors3['name'].' | Phone: '.$competition->Mentors3['phone']) ?></td>
            </tr>
        <?php } ?>
        <tr>
            <th scope="row"><?= __('Number of Participants') ?></th>
            <td><?= $this->Number->format($competition->num_participants) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($competition->date) ?></td>
        </tr>
    </table>
    <div class="row center">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($competition->description)); ?>
    </div>
</div>
