<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';

$user_data =$this->request->getSession()->read('Auth.User');

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>APU Competition Achievements System</title>

    <!-- <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>-->

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->Html->css('https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css') ?>
    <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js') ?>
    <?= $this->Html->script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') ?>

    <!-- new -->
    <?= $this->Html->css('boostrap.min.css') ?>
    <?= $this->Html->css('landing-page.css') ?>
    <?= $this->Html->css('/gcs/css/font-awesome/font-awesome.min.css') ?>
    <?= $this->Html->css('https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic') ?>
    <script src="/gcs/js/jquery.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!-- new -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <style>
    
    .topbarBlue {
        background-color: #008CBA;
    }


    </style>

<!-- new -->

</head>
<body>
    <nav class="top-bar expanded topbarBlue" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <?php if($this->request->getSession()->read('Auth')) { ?>
                    <h1><strong style="color: white; text-decoration: bold;"><?= '&emsp;&emsp;&emsp;A D M I N &emsp; P A N E L' ?></a></h1>
                <?php } else { ?>
                    <h1><strong style="color: white; text-decoration: bold;"><?= 'APU Competition Achievements System' ?></a></h1>
                <?php } ?>

            </li>
        </ul>

  <!--
  <div class="dropdown">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
      Listing Category
    </button>
     <div class="dropdown-menu">
      <?php foreach ($field as $f): ?>
          <a class="dropdown-item"> <?= $this->Html->link($f->name, ['controller'=>'Competitions','action' => 'list', $f->id]) ?></a>
      <?php endforeach; ?>
    </div>
  </div>
  -->

        <div class="top-bar-section">
            <ul class="right">
                <?php if($this->request->getSession()->read('Auth')) { ?>
                    <li>
                    <?= $this->Html->link('HOME', ['controller' => 'competitions', 'action' => 'landing'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>
                    <?= $this->Html->link('COMPETITIONS', ['controller' => 'competitions', 'action' => 'index'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>
                    <?= $this->Html->link('RESULTS', ['controller' => 'results', 'action' => 'index'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>
                    <?= $this->Html->link('MENTORS', ['controller' => 'mentors', 'action' => 'index'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>                    
                    <?= $this->Html->link('SCHOOLS', ['controller' => 'fields', 'action' => 'index'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>                    
                    <?= $this->Html->link('REGISTER', ['controller' => 'users', 'action' => 'register'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>
                   <?= $this->Html->link('EDIT ACCOUNT', ['controller' => 'users', 'action' => 'edit', $user_data['id']], ['class' => 'button,topbarBlue']); ?> 
                    </li>

                    <li>
                    <?= $this->Html->link('LOGOUT', ['controller' => 'users', 'action' => 'logout'], ['class' => 'button,topbarBlue']) ?>
                    </li>

                <?php }else{ ?>
                    <li>
                    <?= $this->Html->link('HOME', ['controller' => 'competitions', 'action' => 'landing'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>
                    <?= $this->Html->link('SUBMIT RESULT', ['controller' => 'results', 'action' => 'publish'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>
                    <?= $this->Html->link('COMPETITIONS', ['controller' => 'competitions', 'action' => 'index'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>                    
                    <?= $this->Html->link('RESULTS', ['controller' => 'results', 'action' => 'index'], ['class' => 'button,topbarBlue']) ?>
                    </li>
                    <li>                    
                    <?= $this->Html->link('LOGIN', ['controller' => 'users', 'action' => 'index'], ['class' => 'button,topbarBlue']) ?>
                     </li>                   
                <?php }?>
            </ul>
        </div>
    </nav>


    <?= $this->Flash->render() ?>


    <div class="container clearfix" style="padding-bottom:3%;">
        <?= $this->fetch('content') ?>
    </div >

    <footer style="text-align: center; color: black; background-color: #34495E ;  margin: 30 30 30 30%;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                
                <span style="font-size: 15px; ">
                    <span style="font-family: arial,helvetica,sans-serif; color: yellow; ">
                        <strong>APU - Rated No.1&nbsp;in Asia and Malaysia for Multicultural Learning Experience. </strong>
                        <br>(*Student Barometer Wave 2017, ‘Studying with people from other cultures’)<br>
                        <strong>APU - Among Highest Rated Emerging Malaysian Universities - 5 STAR (Excellent Rating) -&nbsp; SETARA 2017 </strong>
                    </span>
                </span>
                
                    <p class="copyright medium" style="font-size: 17px; color: white; text-decoration: bold;"><br><strong>Copyright &copy; Group TP038164 (Group Case Study)<br>All Rights Reserved</strong></p>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
