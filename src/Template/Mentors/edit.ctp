<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Mentor $mentor
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Show All Mentors'), ['action' => 'index']) ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mentor->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mentor->id)]
            )
        ?></li>
    </ul>
</nav>
<div class="mentors form large-9 medium-8 columns content">
    <?= $this->Form->create($mentor) ?>
    <fieldset>
        <legend style="color: black;"><?= __('Edit Mentor') ?></legend><br>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('email');
            echo $this->Form->control('phone');
            echo $this->Form->input('fields_id', array("options"=>$field,'class' => 'form-control','label'=>'Schools'));
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
