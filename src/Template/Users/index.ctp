<!DOCTYPE html>
<html lang="en">
<head>
	<title>SYSTEM LOGIN</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/gcs/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/gcs/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/gcs/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/gcs/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/gcs/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/gcs/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/gcs/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/gcs/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/gcs/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/gcs/css/util.css">
	<link rel="stylesheet" type="text/css" href="/gcs/css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(/gcs/images/bg-01.jpg);">
					<span class="login100-form-title-1" style="font-size: 40px;">
						S I G N &nbsp; &nbsp; I N
					</span>
				</div>


<div class="users form">
  <?= $this->Flash->render() ?>
  <?= $this->Form->create() ?>
      <fieldset>
      	<br>
      	<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required"  style=" text-decoration: bold; font-size: 20px;">
          <?= $this->Form->control('email', ['type' => 'email', 'label' => 'Email','class' => 'input100','style' => 'background-color:#C8CAD1;']) ?>
        <br>
          <?= $this->Form->control('password',['class' => 'input100','style' => 'background-color:#C8CAD1;']) ?>
      </fieldset>


  <div class="flex-sb-m w-full p-b-30">
  	<div><a href="/gcs/competitions/landing" class="txt1" style="position: center; padding-left: 190%; font-size: 20px;">
		<strong>BACK TO MENU</strong></a>
	</div>
  </div>
					

  <div class="container-login100-form-btn" style="position: center; padding-bottom: 5%; ">
  	<?= $this->Form->button(__('Login'), ['class' => 'login100-form-btn']); ?>
  </div>
  <?= $this->Form->end() ?>
</div>

	
<!--===============================================================================================-->
	<script src="/gcs/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/gcs/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="/gcs/vendor/bootstrap/js/popper.js"></script>
	<script src="/gcs/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/gcs/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/gcs/vendor/daterangepicker/moment.min.js"></script>
	<script src="/gcs/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="/gcs/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/gcs/js/main.js"></script>

</body>
</html>
