<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Field $field
 */
?>

<br>
    <fieldset>
        <legend style="color: black;"><?= __('Edit Account') ?></legend><br>
            <?= $this->Form->create($user) ?>
        <?= $this->Form->control('name') ?>
        <?= $this->Form->control('password') ?>
        <?= $this->Form->control('email') ?>

    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
